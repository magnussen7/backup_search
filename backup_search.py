#!/usr/bin/python3.6
# coding: utf-8
import argparse
import requests

def search_backup_file(url, file):
    ext_list=['.backup', '.bck', '.old', '.save', '.bak', '.sav', '~', '.copy', '.orig', '.original', '.tmp', '.txt', '.back', '.bkp', '.bac', '.tar', '.gz', '.tar.gz', '.zip', '.rar', '.bz2', '.tar.bz2']
    pre_list = ['backup', 'bck', 'old', 'save', 'bak', 'sav', 'copy', 'orig', 'original', 'tmp', 'back', 'bkp', 'bac']
    join_list = ['.', '_', '-']
    possible_file = []

    for extension in ext_list:
        file_test = url + file + extension
        request = requests.get(file_test)

        if request.status_code == 200:
            print('[+] Possible backup file (200) : ' + str(file_test))
            possible_file.append(file_test)

    for prefix in pre_list:
        for join in join_list:
            file_test = url + prefix + join + file
            request = requests.get(file_test)

            if request.status_code == 200:
                print('[+] Possible backup file (200) : ' + str(file_test))
                possible_file.append(file_test)

    return possible_file

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test for Back up File')
    parser.add_argument('url', type=str, help='URL to test\nExample: https://website.com/')
    parser.add_argument('file', type=str, help='Search for backup of this file\nExample: index.php')
    args = parser.parse_args()

    if args.url[-1] == '/':
        test = args.url + args.file
        url = args.url
    else:
        test = args.url + '/' + args.file
        url = args.url + '/'

    request = requests.get(test)

    if request.status_code == 200:
        print('[+] Searching for back up file.')

        possible_file = search_backup_file(url, args.file)

        print('[+] List of possible file : ')

        for file in possible_file:
            print('\t' + str(file))
    else:
        print('[-] The file or website doesn\'t exist.')