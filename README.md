# backup_search

Automatisation of backup file searching.

## Requirements 
```
    pip3 install argparse requests
```
    
## Usage
```
    python3.6 -url -file
    
    Example : 
        python3.6 https://website.com/ index.php
```

## Extension and Prefix

### Extension : 
    .backup
    .bck
    .old
    .save
    .bak
    .sav
    ~
    .copy
    .orig
    .original
    .tmp
    .txt
    .back
    .bkp
    .bac
    .tar
    .gz
    .tar.gz
    .zip
    .rar
    .bz2
    .tar.bz2

### Prefix :
    Each prefix of this list is concatenated with '.', '-' and '_'
    backup
    bck
    old
    save
    bak
    sav
    copy
    orig
    original
    tmp
    back
    bkp
    bac

